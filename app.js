var express = require("express"),
    app = express(),
    bodyParser = require("body-parser");

app.use(express.static("css"));
app.use(express.static("js"));

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.set("view engine","ejs");

app.get("/", function(req, res) {
    res.render("index");
});

app.listen(process.env.PORT, process.env.IP,function() {
    console.log("Server Listening on PORT", process.env.PORT);
});
